import React, { Component } from "react";
import { Text, View } from "react-native";

class Blink extends Component {
  constructor(props) {
    super(props);
    this.state = { isShowingText: true };

    setInterval(
      () =>
        this.setState(previousState => ({
          isShowingText: !previousState.isShowingText
        })),
      this.props.interval
    );
  }

  render() {
    if (!this.state.isShowingText) {
      return null;
    }
    return <Text>{this.props.text}</Text>;
  }
}

export default class App extends Component {
  render() {
    return (
      <View>
        <Blink text="I love to blink" interval={1000} />
        <Blink text="Yes blinking is so great" interval={500} />
        <Blink text="Why did they ever take this out of HTML" interval={250} />
        <Blink text="Look at me look at me look at me" interval={125} />
      </View>
    );
  }
}
